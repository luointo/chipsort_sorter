## 目录结构
```
│  app.py  # 启动文件                                                                                           
│                                                                                                   
└─apps                                                                                              
    │  __init__.py                                                                                  
    ├─boot  # 用来分别开启每个单独的模块      
    │  │  boot.py                                                                                   
    │  │  __init__.py                                                                               
    │  ├─ui                                                                                         
    │  │  │  boot.py                                                                                
    │  │  │  boot.ui                                                                                
    │  │  │  __init__.py                                                                            
    ├─cameras  # 相机模块目录                                                                                    
    │  │  cameras.py                                                                                
    │  │  thread_camera.py                                                                          
    │  │  __init__.py                                                                               
    │  ├─conf                                                                                       
    │  │      camera_fg.ini                                                                         
    │  │      camera_sr.ini                                                                         
    │  ├─ui                                                                                         
    │  │  │  advanced_setting.py                                                                    
    │  │  │  advanced_setting.ui                                                                    
    │  │  │  cameras.py                                                                             
    │  │  │  cameras.ui                                                                             
    │  │  │  general_setting.py                                                                     
    │  │  │  general_setting.ui                                                                     
    │  │  │  __init__.py                                                                            
    ├─common  # 公共模块，存放公用的函数和类                                                                                       
    │  │  class_qt.py                                                                               
    │  │  func_func.py                                                                              
    │  │  __init__.py                                                                               
    ├─moves  # 四路移动模块                                                                                        
    │  │  func_udp.py                                                                               
    │  │  moves.py                                                                                  
    │  │  thread_move.py                                                                            
    │  │  thread_org.py                                                                             
    │  │  __init__.py                                                                               
    │  ├─conf                                                                                       
    │  │      move.ini                                                                              
    │  ├─ui                                                                                         
    │  │  │  move.py                                                                                
    │  │  │  move.ui                                                                                
    │  │  │  move.ui.bak                                                                            
    │  │  │  setting.py                                                                             
    │  │  │  setting.ui                                                                             
    │  │  │  __init__.py                                                                            
    ├─serials  # 串口模块                                                                                    
    │  │  func_serial.py                                                                            
    │  │  serials.py                                                                                
    │  │  thread_serial.py                                                                          
    │  │  thread_serial_load_data.py                                                                
    │  │  thread_serial_search_location.py                                                          
    │  │  __init__.py                                                                               
    │  ├─conf                                                                                       
    │  │      electromagnet.ini                                                                     
    │  │      io1.txt                                                                               
    │  │      road1.txt                                                                             
    │  │      road2.txt                                                                             
    │  ├─ui                                                                                         
    │  │  │  electromagnet.py                                                                       
    │  │  │  electromagnet.ui                                                                       
    │  │  │  io_test.py                                                                             
    │  │  │  io_test.ui                                                                             
    │  │  │  serials.py                                                                             
    │  │  │  serials.ui                                                                             
    │  │  │  __init__.py                                                                            
    ├─sorter  # 主程序                                                                                       
    │  │  func_check.py                                                                             
    │  │  img.png                                                                                   
    │  │  sorter.py                                                                                 
    │  │  __init__.py                                                                               
    │  ├─ui                                                                                         
    │  │  │  login.py                                                                               
    │  │  │  login.ui                                                                               
    │  │  │  sorter.py                                                                              
    │  │  │  sorter.ui                                                                              
    │  │  │  __init__.py                                                                            
                                                                                                    
```

- 以ui命名的文件夹存放Qt界面文件
- func 开头的文件里面放的函数
- thread 开头的文件，线程
- 每个文件夹下和文件夹同名的文件可以单独启动模块