# -*- coding: utf-8 -*-
__author__ = 'luointo'
__date__ = '2018/6/20 18:16'

import sys
import re
import pathlib
import time
from PyQt5.QtWidgets import (QApplication, QMessageBox, QSplashScreen, QProgressBar, qApp, QMainWindow, QTextEdit, QDesktopWidget)
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPixmap
from apps.common.class_qt import BaseQDialog
from apps.sorter.ui.login import Ui_Dialog_Login
from apps.sorter.ui.sorter import Ui_MainWindow_Sorter
from apps.sorter.func_check import (check_move, check_serial, check_camera, check_file)


class Login(BaseQDialog, Ui_Dialog_Login):
    def __init__(self):
        super(Login, self).__init__()
        self.setupUi(self)
        self.pushButton_login.clicked.connect(self.func_login)
        # self.lineEdit_work_number.returnPressed.connect(self.func_login)
        self.lineEdit_work_number.setText("s111")
        self.is_admin = False

    def func_login(self):
        work_number = self.lineEdit_work_number.text().strip()
        if not work_number:
            return
        work_number = work_number.upper()
        if work_number == "ADMIN":
            self.is_admin = True
            self.close()
            return
        if not re.match(r"^(S\d{3,4}|N\d{4})$", work_number):
            self.info("工号格式不正确")
            return
        self.close()

        # # 检查开始
        # img = str(pathlib.Path(__file__).parent / "img.png")
        # splash_pix = QPixmap(img)
        # splash = QSplashScreen(splash_pix, Qt.WindowStaysOnTopHint)
        # splash.setWindowFlags(Qt.WindowStaysOnTopHint | Qt.FramelessWindowHint)
        # splash.setEnabled(False)
        # progress = QProgressBar(splash)
        # progress.setRange(0, 40)
        # progress.setGeometry(0, splash_pix.height() - 50, splash_pix.width(), 20)
        # splash.show()
        #
        # # splash.showMessage(u'正在加载初始界面……', Qt.AlignCenter, Qt.yellow)  # 设置文字在正中央
        # # splash.showMessage("<h1><font color='green'>检查中。。。</font></h1>", Qt.AlignTop | Qt.AlignCenter, Qt.black)
        #
        # func_dict = [
        #     {"func_name": check_camera, "arguments": 0, "description": "正在检查视频1..."},
        #     {"func_name": check_camera, "arguments": 1, "description": "正在检查视频2..."},
        #     # {"func_name": check_move, "arguments": None, "description": "正在检查4路..."},
        #     # {"func_name": check_file, "arguments": LOCAL_CONF, "description": "正在检查本地配置文件..."},
        #     # {"func_name": check_serial, "arguments": SERIAL_NAME, "description": "正在检查串口..."},
        # ]
        #
        # for i, func in enumerate(func_dict):
        #     splash.showMessage("<h1><font color='green'>" + func["description"] + "</font></h1>", Qt.AlignCenter, Qt.black)
        #     result = func["func_name"](func["arguments"])
        #     if result is not True:
        #         pass
        #         # 检查
        #         # splash.close()
        #         # self.info(result)
        #         # return
        #     for x in range(i * 10, (i + 1) * 10):
        #         progress.setValue(x)
        #         t = time.time()
        #         while time.time() < t + 0.01:
        #             qApp.processEvents()
        #
        # # 检查正常后
        #
        # sorter = Sorter()
        # sorter.show()
        #
        # splash.finish(sorter)  # 启动画面完成启动


class Sorter(QMainWindow, Ui_MainWindow_Sorter):
    def __init__(self):
        super(Sorter, self).__init__()
        self.setupUi(self)


def login():
    app = QApplication(sys.argv)
    ui = Login()
    ui.show()
    app.exec_()
    return ui


def run():
    login_ui = login()
    app = QApplication(sys.argv)
    # 检查开始
    img = str(pathlib.Path(__file__).parent / "img.png")
    splash_pix = QPixmap(img)
    splash = QSplashScreen(splash_pix, Qt.WindowStaysOnTopHint)
    splash.setWindowFlags(Qt.WindowStaysOnTopHint | Qt.FramelessWindowHint)
    splash.setEnabled(False)
    progress = QProgressBar(splash)
    progress.setRange(0, 40)
    progress.setGeometry(0, splash_pix.height() - 50, splash_pix.width(), 20)
    splash.show()
    func_dict = [
        {"func_name": check_camera, "arguments": 0, "description": "正在检查视频1..."},
        {"func_name": check_camera, "arguments": 1, "description": "正在检查视频2..."},
        {"func_name": check_move, "arguments": None, "description": "正在检查4路..."},
        # {"func_name": check_file, "arguments": LOCAL_CONF, "description": "正在检查本地配置文件..."},
        {"func_name": check_serial, "arguments": None, "description": "正在检查串口..."},
    ]
    errs = []  # 错误信息
    for i, func in enumerate(func_dict):
        splash.showMessage("<h1><font color='green'>" + func["description"] + "</font></h1>", Qt.AlignCenter, Qt.black)
        arguments = func["arguments"]
        if arguments is None:
            result = func["func_name"]()
        else:
            result = func["func_name"](arguments)
        if result is not True:
            errs.append(result)
        for x in range(i * 10, (i + 1) * 10):
            progress.setValue(x)
            t = time.time()
            while time.time() < t + 0.01:
                qApp.processEvents()

    splash.close()
    if errs:  # 检查有错误
        ui = QTextEdit()
        ui.setWindowTitle("错误信息")
        ui.resize(500, 300)
        for i, err in enumerate(errs, 1):
            ui.append("错误{} : {}".format(i, err))
        ui.show()
        splash.finish(ui)  # 启动画面完成启动
        app.exec_()
    else:  # 检查正常
        ui = Sorter()
        ui.show()
        splash.finish(ui)
        sys.exit(app.exec_())
    if login_ui.is_admin:
        ui = Sorter()
        ui.show()
        sys.exit(app.exec_())


if __name__ == '__main__':
    run()
