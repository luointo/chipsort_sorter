# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'sorter.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow_Sorter(object):
    def setupUi(self, MainWindow_Sorter):
        MainWindow_Sorter.setObjectName("MainWindow_Sorter")
        MainWindow_Sorter.resize(800, 600)
        self.centralwidget = QtWidgets.QWidget(MainWindow_Sorter)
        self.centralwidget.setObjectName("centralwidget")
        MainWindow_Sorter.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow_Sorter)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 23))
        self.menubar.setObjectName("menubar")
        MainWindow_Sorter.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow_Sorter)
        self.statusbar.setObjectName("statusbar")
        MainWindow_Sorter.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow_Sorter)
        QtCore.QMetaObject.connectSlotsByName(MainWindow_Sorter)

    def retranslateUi(self, MainWindow_Sorter):
        _translate = QtCore.QCoreApplication.translate
        MainWindow_Sorter.setWindowTitle(_translate("MainWindow_Sorter", "分选"))

