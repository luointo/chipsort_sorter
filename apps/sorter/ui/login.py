# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'login.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog_Login(object):
    def setupUi(self, Dialog_Login):
        Dialog_Login.setObjectName("Dialog_Login")
        Dialog_Login.resize(360, 358)
        self.label_work_number = QtWidgets.QLabel(Dialog_Login)
        self.label_work_number.setGeometry(QtCore.QRect(110, 90, 151, 51))
        font = QtGui.QFont()
        font.setPointSize(20)
        self.label_work_number.setFont(font)
        self.label_work_number.setObjectName("label_work_number")
        self.lineEdit_work_number = QtWidgets.QLineEdit(Dialog_Login)
        self.lineEdit_work_number.setGeometry(QtCore.QRect(80, 160, 191, 61))
        font = QtGui.QFont()
        font.setPointSize(23)
        self.lineEdit_work_number.setFont(font)
        self.lineEdit_work_number.setText("")
        self.lineEdit_work_number.setObjectName("lineEdit_work_number")
        self.pushButton_login = QtWidgets.QPushButton(Dialog_Login)
        self.pushButton_login.setGeometry(QtCore.QRect(120, 260, 111, 51))
        font = QtGui.QFont()
        font.setPointSize(20)
        self.pushButton_login.setFont(font)
        self.pushButton_login.setObjectName("pushButton_login")

        self.retranslateUi(Dialog_Login)
        QtCore.QMetaObject.connectSlotsByName(Dialog_Login)

    def retranslateUi(self, Dialog_Login):
        _translate = QtCore.QCoreApplication.translate
        Dialog_Login.setWindowTitle(_translate("Dialog_Login", "登录界面"))
        self.label_work_number.setText(_translate("Dialog_Login", "请输入工号"))
        self.pushButton_login.setText(_translate("Dialog_Login", "登录"))

