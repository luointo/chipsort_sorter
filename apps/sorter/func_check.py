# -*- coding: utf-8 -*-
__author__ = 'luointo'
__date__ = '2018/6/21 8:49'

"""
1. 检查4路控制器是否正常
2. 检查摄像头是否正常
3. 检查配置文件是否存在
"""

import os
import cv2
import socket
import serial
import serial.tools.list_ports
from apps.moves.func_udp import get_send_address, get_receive_address


def check_move():
    """
    检查4路控制器端口
    :return:
    """
    try:
        send_address = get_send_address()
    except Exception as e:
        return "四路地址获取失败" + str(e)
    s_send = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        s_send.sendto("1;".encode(), send_address)  # 发送数据
    except Exception as e:
        return "向四路发送数据失败" + str(e)
    finally:
        s_send.close()
    receive_address = get_receive_address()
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        s.bind(receive_address)
    except Exception as e:
        return "四路绑定失败" + str(e)
    try:
        s.settimeout(1)
        s.recvfrom(1024)  # 接收数据
    except Exception as e:
        s.settimeout(None)
        return "四路接收数据超时" + str(e)
    else:
        return True
    finally:
        s.close()


def check_camera(index):
    """
    检查摄像头
    :param index:
    :return:
    """
    cap = cv2.VideoCapture(int(index))
    if not cap.isOpened():
        cap.release()  # 释放VideoCapture对象
        return str(int(index) + 1) + "号摄像头打开失败"
    else:
        cap.release()
        return True


def check_file(filename):
    """
    检查文件是否存在
    :param filename:
    :return:
    """
    result = os.path.isfile(filename)
    if result:
        return True
    else:
        return "配置文件 {} 不存在".format(filename)


def check_serial():
    """
    检查串口通信
    :return:
    """
    plist = serial.tools.list_ports.comports()
    if not plist:
        return "没有串口设备连接"
    try:
        ser = serial.Serial(plist[0].device, baudrate=115200, timeout=1)
    except Exception as e:
        return "串口连接失败" + str(e)
    else:
        ser.close()
        return True


if __name__ == '__main__':
    import time

    t = time.time()

    a = check_serial()
    print(a)

    print(time.time() - t)
