# -*- coding: utf-8 -*-
__author__ = 'luointo'
__date__ = '2018/6/20 13:31'

from PyQt5.QtWidgets import QDialog, QMessageBox


class BaseQDialog(QDialog):

    @staticmethod
    def info(text):
        # 弹出信息提示框
        box = QMessageBox()
        box.setWindowTitle("提示")
        box.setIcon(QMessageBox.Information)
        box.setText(text)
        y = box.addButton("确定", QMessageBox.AcceptRole)
        box.setDefaultButton(y)
        box.exec()
