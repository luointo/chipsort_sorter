# -*- coding: utf-8 -*-
__author__ = 'luointo'
__date__ = '2018/6/14 11:52'

import threading
import configparser
import functools


def thread_get_obj(thread_name):
    for thread in threading.enumerate():
        if thread.getName() == thread_name:
            return thread


def file_get_cfg(filename):
    cfg = configparser.ConfigParser()
    cfg.read(filename, encoding="utf-8")
    return cfg


def file_cfg_save(filename, cfg):
    with open(file=filename, mode="w", encoding="utf-8") as f:
        cfg.write(f)

