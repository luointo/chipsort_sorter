# -*- coding: utf-8 -*-
__author__ = 'luointo'
__date__ = '2018/1/15 9:02'

import threading
import cv2

import configparser

import numpy as np
import time
import os
import platform


class ThreadCamera(threading.Thread):
    """
    从相机获取图片处理
    """

    def __init__(self, thread_name, ui):
        threading.Thread.__init__(self, name=thread_name)
        self.thread_name = thread_name
        self.__flag = threading.Event()  # 用于暂停线程的标识
        self.__flag.set()  # 设置为True
        self.begin = True
        self.ui = ui
        self.signal = self.ui.signal.signal_video  # 定义更换图片信号
        self.current_image = None  # 当前图片
        self.capture = None
        self.init_camera()

    def init_camera(self):
        # 初始化相机
        if self.thread_name == "camera_sr":
            conf_file = self.ui.sr_conf_file
        else:
            conf_file = self.ui.fg_conf_file
        cfg = configparser.ConfigParser()
        cfg.read(conf_file, encoding="utf-8")
        index = cfg.getint("video", "index")
        self.capture = cv2.VideoCapture(index)
        self.capture.read()

    def run(self):
        self.get_image()

    def pause(self):
        # 暂停
        self.__flag.clear()
        self.capture.release()

    def recover(self):
        # 恢复
        self.__flag.set()

    def get_image(self):
        # 获取图片
        while self.begin:
            self.__flag.wait()
            ret, frame = self.capture.read()  # 返回两个值，ret表示读取是否成功，frame为读取的帧内容
            if frame is None:  # 判断传入的帧是否为空，为空则退出
                break
            self.current_image = frame
            self.signal.emit(self.thread_name, self.current_image)
