# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'general_setting.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog_General_Setting(object):
    def setupUi(self, Dialog_General_Setting):
        Dialog_General_Setting.setObjectName("Dialog_General_Setting")
        Dialog_General_Setting.resize(557, 192)
        self.layoutWidget = QtWidgets.QWidget(Dialog_General_Setting)
        self.layoutWidget.setGeometry(QtCore.QRect(60, 40, 414, 25))
        self.layoutWidget.setObjectName("layoutWidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.layoutWidget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label_exposure = QtWidgets.QLabel(self.layoutWidget)
        self.label_exposure.setObjectName("label_exposure")
        self.horizontalLayout.addWidget(self.label_exposure)
        self.horizontalScrollBar_exposure = QtWidgets.QScrollBar(self.layoutWidget)
        self.horizontalScrollBar_exposure.setMinimumSize(QtCore.QSize(300, 0))
        self.horizontalScrollBar_exposure.setOrientation(QtCore.Qt.Horizontal)
        self.horizontalScrollBar_exposure.setObjectName("horizontalScrollBar_exposure")
        self.horizontalLayout.addWidget(self.horizontalScrollBar_exposure)
        self.lcdNumber_exposure = QtWidgets.QLCDNumber(self.layoutWidget)
        self.lcdNumber_exposure.setObjectName("lcdNumber_exposure")
        self.horizontalLayout.addWidget(self.lcdNumber_exposure)
        self.layoutWidget_2 = QtWidgets.QWidget(Dialog_General_Setting)
        self.layoutWidget_2.setGeometry(QtCore.QRect(60, 90, 414, 25))
        self.layoutWidget_2.setObjectName("layoutWidget_2")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.layoutWidget_2)
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label_contrast = QtWidgets.QLabel(self.layoutWidget_2)
        self.label_contrast.setObjectName("label_contrast")
        self.horizontalLayout_2.addWidget(self.label_contrast)
        self.horizontalScrollBar_contrast = QtWidgets.QScrollBar(self.layoutWidget_2)
        self.horizontalScrollBar_contrast.setMinimumSize(QtCore.QSize(300, 0))
        self.horizontalScrollBar_contrast.setOrientation(QtCore.Qt.Horizontal)
        self.horizontalScrollBar_contrast.setObjectName("horizontalScrollBar_contrast")
        self.horizontalLayout_2.addWidget(self.horizontalScrollBar_contrast)
        self.lcdNumber_contrast = QtWidgets.QLCDNumber(self.layoutWidget_2)
        self.lcdNumber_contrast.setObjectName("lcdNumber_contrast")
        self.horizontalLayout_2.addWidget(self.lcdNumber_contrast)

        self.retranslateUi(Dialog_General_Setting)
        QtCore.QMetaObject.connectSlotsByName(Dialog_General_Setting)

    def retranslateUi(self, Dialog_General_Setting):
        _translate = QtCore.QCoreApplication.translate
        Dialog_General_Setting.setWindowTitle(_translate("Dialog_General_Setting", "基本设置"))
        self.label_exposure.setText(_translate("Dialog_General_Setting", "曝  光"))
        self.label_contrast.setText(_translate("Dialog_General_Setting", "对比度"))

