# -*- coding: utf-8 -*-
__author__ = 'luointo'
__date__ = '2018/6/7 10:29'

import sys
import cv2
import time
import configparser
import pathlib
from PyQt5.QtWidgets import (QApplication, QDialog, QMessageBox)
from PyQt5.QtCore import (QObject, pyqtSignal, pyqtSlot, Qt)
from PyQt5.QtGui import (QImage, QPixmap)
from apps.cameras.ui.cameras import Ui_Dialog_Cameras
from apps.cameras.thread_camera import ThreadCamera
from apps.cameras.ui.general_setting import Ui_Dialog_General_Setting
from apps.cameras.ui.advanced_setting import Ui_Dialog_Advanced_Setting
from apps.common.func_func import thread_get_obj
from apps.common.class_qt import BaseQDialog


class Signal(QObject):
    """
    信号
    """

    # 视频信号
    # signal_video = pyqtSignal(str, object, float, str)
    signal_video = pyqtSignal(str, object)

    def __init__(self, parent):
        super(Signal, self).__init__(parent=parent)
        self.signal_video[str, object].connect(self.slot_show_image)

        self.image_count_sr = 0  # 统计取了多少张图片
        self.image_count_fg = 0  # 统计取了多少张图片
        self.fps_sr = 0
        self.fps_fg = 0
        self.camera_sr_start_time = time.time()
        self.camera_fg_start_time = time.time()

    @pyqtSlot(str, object)
    def slot_show_image(self, who, image):
        """
        显示视频画面
        """
        frame = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        frame = cv2.resize(frame, (640, 480))
        image = QImage(frame, frame.shape[1], frame.shape[0], frame.strides[0], QImage.Format_RGB888)

        if who == "camera_sr":

            # 张数
            self.image_count_sr += 1
            if self.image_count_sr == 100000:
                self.image_count_sr = 1
            self.parent().lcdNumber_camera_count_sr.display(self.image_count_sr)

            # fps
            self.fps_sr += 1
            if self.fps_sr == 30:
                fps = round(30.0 / (time.time() - self.camera_sr_start_time), 2)
                self.camera_sr_start_time = time.time()
                self.fps_sr = 0
                self.parent().lcdNumber_fps_sr.display(fps)

            self.parent().label_camera_sr.setPixmap(QPixmap.fromImage(image))
        else:
            # 张数
            self.image_count_fg += 1
            if self.image_count_fg == 100000:
                self.image_count_fg = 1
            self.parent().lcdNumber_camera_count_fg.display(self.image_count_fg)

            # fps
            self.fps_fg += 1
            if self.fps_fg == 30:
                fps = round(30.0 / (time.time() - self.camera_fg_start_time), 2)
                self.camera_fg_start_time = time.time()
                self.fps_fg = 0
                self.parent().lcdNumber_fps_fg.display(fps)

            self.parent().label_camera_fg.setPixmap(QPixmap.fromImage(image))


class GeneralSetting(BaseQDialog, Ui_Dialog_General_Setting):
    def __init__(self, parent, sr_or_fg):
        super(GeneralSetting, self).__init__(parent=parent)
        self.setupUi(self)
        self.horizontalScrollBar_exposure.valueChanged[int].connect(lambda value: self.scroll_event("exposure", value))
        self.horizontalScrollBar_contrast.valueChanged[int].connect(lambda value: self.scroll_event("contrast", value))
        self.sr_or_fg = sr_or_fg
        self.conf_file = None  # 配置文件
        self.init()

    def init(self):
        if self.sr_or_fg == "sr":
            self.conf_file = self.parent().sr_conf_file
        else:
            self.conf_file = self.parent().fg_conf_file
        cfg = configparser.ConfigParser()
        cfg.read(self.conf_file, encoding="utf-8")
        # 初始化ScrollBar的范围和lcdNumber的值
        self.horizontalScrollBar_exposure.setRange(-50, 50)
        self.horizontalScrollBar_contrast.setRange(0, 300)
        self.lcdNumber_exposure.display(cfg.getint("basic", "exposure"))
        self.lcdNumber_contrast.display(cfg.getint("basic", "contrast"))

    def display(self):
        self.exec_()

    def scroll_event(self, name, value):
        if name == "exposure":
            label = 15
            lcd = self.lcdNumber_exposure
        else:
            label = 11
            lcd = self.lcdNumber_contrast
        camera = thread_get_obj("camera_" + self.sr_or_fg)
        camera.capture.set(label, value)
        get_set_val = int(camera.capture.get(label))
        lcd.display(get_set_val)
        cfg = configparser.ConfigParser()
        cfg.read(self.conf_file, encoding="utf-8")
        cfg.set("basic", name, str(get_set_val))
        with open(file=self.conf_file, mode="w", encoding="utf-8") as f:
            cfg.write(f)


class AdvancedSetting(BaseQDialog, Ui_Dialog_Advanced_Setting):
    def __init__(self, parent, sr_or_fg):
        super(AdvancedSetting, self).__init__(parent=parent)
        self.setupUi(self)
        self.horizontalScrollBar_brightness.valueChanged[int].connect(lambda value: self.scroll_event("brightness", value))
        self.horizontalScrollBar_saturation.valueChanged[int].connect(lambda value: self.scroll_event("saturation", value))
        self.horizontalScrollBar_hue.valueChanged[int].connect(lambda value: self.scroll_event("hue", value))
        self.horizontalScrollBar_gain.valueChanged[int].connect(lambda value: self.scroll_event("gain", value))
        self.sr_or_fg = sr_or_fg
        self.conf_file = None  # 配置文件
        self.init()

    def init(self):
        if self.sr_or_fg == "sr":
            self.conf_file = self.parent().sr_conf_file
        else:
            self.conf_file = self.parent().fg_conf_file
        cfg = configparser.ConfigParser()
        cfg.read(self.conf_file, encoding="utf-8")
        # 初始化ScrollBar的范围和lcdNumber的值
        self.horizontalScrollBar_brightness.setRange(-50, 50)
        self.horizontalScrollBar_saturation.setRange(0, 300)
        self.horizontalScrollBar_hue.setRange(0, 300)
        self.horizontalScrollBar_gain.setRange(0, 300)
        self.lcdNumber_brightness.display(cfg.getint("advanced", "brightness"))
        self.lcdNumber_saturation.display(cfg.getint("advanced", "saturation"))
        self.lcdNumber_hue.display(cfg.getint("advanced", "hue"))
        self.lcdNumber_gain.display(cfg.getint("advanced", "gain"))

    def display(self):
        self.exec_()

    def scroll_event(self, name, value):
        if name == "brightness":
            label = 10
            lcd = self.lcdNumber_brightness
        elif name == "saturation":
            label = 12
            lcd = self.lcdNumber_saturation
        elif name == "hue":
            label = 13
            lcd = self.lcdNumber_hue
        else:
            label = 14
            lcd = self.lcdNumber_gain
        camera = thread_get_obj("camera_" + self.sr_or_fg)
        camera.capture.set(label, value)
        get_set_val = int(camera.capture.get(label))
        lcd.display(get_set_val)
        cfg = configparser.ConfigParser()
        cfg.read(self.conf_file, encoding="utf-8")
        cfg.set("advanced", name, str(get_set_val))
        with open(file=self.conf_file, mode="w", encoding="utf-8") as f:
            cfg.write(f)


class Cameras(BaseQDialog, Ui_Dialog_Cameras):

    def __init__(self, sr_conf_file, fg_conf_file, parent=None):
        super(Cameras, self).__init__(parent=parent)
        self.setupUi(self)
        self.sr_conf_file = sr_conf_file
        self.fg_conf_file = fg_conf_file
        self.signal = Signal(parent=self)
        self.pushButton_camera_set_sr.clicked.connect(lambda: self.func_camera_set("sr", "general"))
        self.pushButton_camera_set_fg.clicked.connect(lambda: self.func_camera_set("fg", "general"))
        self.pushButton_camera_advanced_set_sr.clicked.connect(lambda: self.func_camera_set("sr", "advanced"))
        self.pushButton_camera_advanced_set_fg.clicked.connect(lambda: self.func_camera_set("fg", "advanced"))
        self.pushButton_camera_switchover.clicked.connect(self.func_camera_switchover)

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Escape:
            self.close()

    def closeEvent(self, event):
        box = QMessageBox()
        box.setWindowTitle('提示')
        box.setIcon(QMessageBox.Question)
        box.setText('您确定要退出吗?')
        box.addButton('确定', QMessageBox.AcceptRole)
        no = box.addButton('取消', QMessageBox.RejectRole)
        box.setDefaultButton(no)
        reply = box.exec_()
        if reply == QMessageBox.AcceptRole:
            event.accept()
            camera_sr = thread_get_obj("camera_sr")
            camera_sr.begin = False
            camera_fg = thread_get_obj("camera_fg")
            camera_fg.begin = False

            if self.parent():
                self.parent().pushButton_camera.setDisabled(False)
        else:
            event.ignore()

    def func_camera_set(self, sr_or_fg, general_or_advanced):
        if general_or_advanced == "general":
            ui = GeneralSetting(parent=self, sr_or_fg=sr_or_fg)
        else:
            ui = AdvancedSetting(parent=self, sr_or_fg=sr_or_fg)
        ui.display()

    def func_camera_switchover(self):
        # 原料和成品互换
        cfg_sr = configparser.ConfigParser()
        cfg_fg = configparser.ConfigParser()
        cfg_sr.read(self.sr_conf_file, encoding="utf-8")
        cfg_fg.read(self.fg_conf_file, encoding="utf-8")
        index_sr = cfg_sr.getint("video", "index")
        index_fg = cfg_fg.getint("video", "index")
        camera_sr = thread_get_obj("camera_sr")
        camera_fg = thread_get_obj("camera_fg")
        camera_sr.pause()
        camera_fg.pause()
        camera_sr.capture = cv2.VideoCapture(index_fg)
        camera_fg.capture = cv2.VideoCapture(index_sr)
        camera_sr.recover()
        camera_fg.recover()
        cfg_sr.set("video", "index", str(index_fg))
        cfg_fg.set("video", "index", str(index_sr))
        with open(file=self.sr_conf_file, mode="w", encoding="utf-8") as f_sr:
            cfg_sr.write(f_sr)
        with open(file=self.fg_conf_file, mode="w", encoding="utf-8") as f_fg:
            cfg_fg.write(f_fg)


def run(alone=True, sr_conf_file=None, fg_conf_file=None, parent=None):
    """
    启动
    :param alone: 独自启动不依赖其他QT
    :param sr_conf_file: 原料配置文件路径
    :param fg_conf_file: 成品配置文件路径
    :param parent: 依赖的父类
    :return:
    """
    app = None
    if alone:
        app = QApplication(sys.argv)
    p = pathlib.Path(__file__).parent
    if sr_conf_file is None:
        sr_conf_file = p / "conf/camera_sr.ini"
    if fg_conf_file is None:
        fg_conf_file = p / "conf/camera_fg.ini"
    cameras = Cameras(sr_conf_file, fg_conf_file, parent)
    thread_camera_sr = ThreadCamera("camera_sr", cameras)
    thread_camera_sr.start()
    thread_camera_fg = ThreadCamera("camera_fg", cameras)
    thread_camera_fg.start()
    cameras.show()
    if alone:
        sys.exit(app.exec_())


if __name__ == '__main__':
    from apps import cameras as aa
    aa.run(alone=True)
