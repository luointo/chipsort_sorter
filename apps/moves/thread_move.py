# -*- coding: utf-8 -*-
__author__ = 'luointo'
__date__ = '2018/6/11 12:05'

import threading
import socket
import time
import pathlib
import configparser

from apps.moves.func_udp import udp_sned_data


class ThreadMove(threading.Thread):
    """
    负责接收4路的坐标数据
    """

    def __init__(self, thread_name, address, signal):
        threading.Thread.__init__(self, name=thread_name)
        self.address = address
        self.signal = signal
        self.flag = True
        self.begin = True
        self.s_receive = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        try:
            self.s_receive.bind(self.address)
        except Exception as e:
            self.flag = False
            print("4路绑定失败", str(e))

        # s = "A1,2000,38000,300000,A2,1500,38000,300000,A3,1500,38000,300000,A4,1500,38000,300000," \
        #     "S1,5000000,140000000,S2,5000000,120000000,S3,5000000,140000000,S4,5000000,140000000,"
        # udp_sned_4_road(s)

        self.s_receive.settimeout(2)  # 设置超时时间为2s
        self.points = dict()

    def run(self):
        self.work()

    def work(self):
        if not self.flag:
            return
        while self.begin:
            try:
                data, _ = self.s_receive.recvfrom(1024)  # 接收数据
            except Exception as e:
                self.begin = False
                print("4路接收数据超时: " + str(e))
            else:
                self.points["x"] = self.convert(data[4:8])
                self.points["y"] = self.convert(data[8:12])
                self.points["a"] = self.convert(data[12:16])
                self.points["b"] = self.convert(data[16:20])
                time.sleep(0.0001)
                self.signal.emit(self.points)

    @staticmethod
    def convert(data):
        """
        把接收到的udp数据坐标解析
        :param data:
        :return:
        """
        # 计算坐标
        a, b, c, d = data
        result = a + b * 256 + c * 65536
        if d > 128:
            result = result + (d and 127) * 16777216
            result = result | -2147483648
        else:
            result = result or d * 16777216
        return result
