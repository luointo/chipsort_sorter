# -*- coding: utf-8 -*-
__author__ = 'luointo'
__date__ = '2018/6/11 11:45'

import sys
import pathlib
from PyQt5.QtWidgets import (QApplication, QDialog, QMessageBox, QAbstractButton)
from PyQt5.QtCore import (QObject, pyqtSignal, pyqtSlot, Qt, QEvent, QRegExp)
from PyQt5.QtGui import QRegExpValidator
from apps.moves.ui.move import Ui_Dialog_Move
from apps.moves.ui.setting import Ui_Dialog_Setting
from apps.moves.thread_move import ThreadMove
from apps.moves.thread_org import ThreadOrg
from apps.moves.func_udp import udp_move_only, udp_move, udp_sned_data
from apps.common.func_func import thread_get_obj, file_get_cfg, file_cfg_save
from apps.common.class_qt import BaseQDialog


class Signal(QObject):
    """
    信号
    """
    signal_move = pyqtSignal(dict)
    signal_org = pyqtSignal(object)

    def __init__(self, parent):
        super(Signal, self).__init__(parent=parent)
        self.signal_move.connect(self.slot_move)
        self.signal_org.connect(self.slot_org)

    @pyqtSlot(dict)
    def slot_move(self, points):
        current_xy_text = self.parent().lineEdit_current_xy.text()
        current_ab_text = self.parent().lineEdit_current_ab.text()
        xy_text = str(points["x"]) + "," + str(points["y"])
        ab_text = str(points["a"]) + "," + str(points["b"])
        if current_xy_text != xy_text:
            self.parent().lineEdit_current_xy.setText(xy_text)
        if current_ab_text != ab_text:
            self.parent().lineEdit_current_ab.setText(ab_text)

    @pyqtSlot(object)
    def slot_org(self, btn):
        btn.setDisabled(False)


class Setting(BaseQDialog, Ui_Dialog_Setting):
    def __init__(self, conf_file=None, parent=None):
        super(Setting, self).__init__(parent=parent)
        self.setupUi(self)
        self.conf_file = conf_file
        self.pushButton_save.clicked.connect(self.save_conf)
        self.pushButton_cancel.clicked.connect(self.cancel_conf)
        self.motor_kv = {
            "a1_1": self.lineEdit_a1_1,
            "a1_2": self.lineEdit_a1_2,
            "a1_3": self.lineEdit_a1_3,
            "a2_1": self.lineEdit_a2_1,
            "a2_2": self.lineEdit_a2_2,
            "a2_3": self.lineEdit_a2_3,
            "a3_1": self.lineEdit_a3_1,
            "a3_2": self.lineEdit_a3_2,
            "a3_3": self.lineEdit_a3_3,
            "a4_1": self.lineEdit_a4_1,
            "a4_2": self.lineEdit_a4_2,
            "a4_3": self.lineEdit_a4_3,
            "s1_1": self.lineEdit_s1_1,
            "s1_2": self.lineEdit_s1_2,
            "s2_1": self.lineEdit_s2_1,
            "s2_2": self.lineEdit_s2_2,
            "s3_1": self.lineEdit_s3_1,
            "s3_2": self.lineEdit_s3_2,
            "s4_1": self.lineEdit_s4_1,
            "s4_2": self.lineEdit_s4_2,
        }
        self.boundary_kv = {
            "x_min": self.lineEdit_x_min,
            "x_max": self.lineEdit_x_max,
            "y_min": self.lineEdit_y_min,
            "y_max": self.lineEdit_y_max,
            "a_min": self.lineEdit_a_min,
            "a_max": self.lineEdit_a_max,
            "b_min": self.lineEdit_b_min,
            "b_max": self.lineEdit_b_max,
        }
        self.org_kv = {
            "x": self.lineEdit_org_x,
            "y": self.lineEdit_org_y,
            "a": self.lineEdit_org_a,
            "b": self.lineEdit_org_b,
        }
        self.kv = {
            "motor": self.motor_kv,
            "boundary": self.boundary_kv,
            "org": self.org_kv,
        }
        self.init()

    def init(self):
        if self.conf_file is None:
            self.conf_file = pathlib.Path(__file__).parent / "conf/move.ini"
        cfg = file_get_cfg(self.conf_file)
        regex1 = QRegExp(r"^\d{10}$")
        regex2 = QRegExp(r"^-?\d{10}$")
        for section, kv in self.kv.items():
            for k, v in kv.items():
                if section == "motor":
                    regex = regex1
                else:
                    regex = regex2
                v.setValidator(QRegExpValidator(regex, self))
                v.setText(cfg.get(section, k))

    def display(self):
        self.exec_()

    def cancel_conf(self):
        cfg = file_get_cfg(self.conf_file)
        for section, kv in self.kv.items():
            for k, v in kv.items():
                v.setText(cfg.get(section, k))

    def save_conf(self):
        cfg = file_get_cfg(self.conf_file)
        motor_data = dict()
        for section, kv in self.kv.items():
            for k, v in kv.items():
                val = v.text().strip()
                cfg.set(section, k, val)
                if section == "motor":
                    motor_data[k] = val
        file_cfg_save(self.conf_file, cfg)
        # 更新马达参数
        data = "A1,{motor_data[a1_1]},{motor_data[a1_2]},{motor_data[a1_3]}," \
               "A2,{motor_data[a2_1]},{motor_data[a2_2]},{motor_data[a2_3]}," \
               "A3,{motor_data[a3_1]},{motor_data[a3_2]},{motor_data[a3_3]}," \
               "A4,{motor_data[a4_1]},{motor_data[a4_2]},{motor_data[a4_3]}," \
               "S1,{motor_data[s1_1]},{motor_data[s1_2]}," \
               "S2,{motor_data[s2_1]},{motor_data[s2_2]}," \
               "S3,{motor_data[s3_1]},{motor_data[s3_2]}," \
               "S4,{motor_data[s4_1]},{motor_data[s4_2]},".format(motor_data=motor_data)
        udp_sned_data(data)
        self.info("保存成功")


class Moves(BaseQDialog, Ui_Dialog_Move):
    """
    主界面
    """

    def __init__(self, conf_file=None, parent=None):
        super(Moves, self).__init__(parent=parent)
        self.setupUi(self)
        self.conf_file = conf_file
        self.signal = Signal(parent=self)
        self.installEventFilter(self)
        self.buttonGroup_keyboard_move.buttonClicked[QAbstractButton].connect(lambda button: self.func_keyboard_btn(button))
        self.pushButton_move_xy.clicked.connect(lambda: self.func_get_value_move("sr"))
        self.pushButton_move_ab.clicked.connect(lambda: self.func_get_value_move("fg"))
        self.pushButton_setting.clicked.connect(self.func_conf)
        self.pushButton_org_zero.clicked.connect(lambda: self.func_org("org_zero"))
        self.pushButton_org_set.clicked.connect(lambda: self.func_org("org_set"))
        self.init()

    def init(self):
        self.pushButton_free.setDisabled(True)
        cfg = file_get_cfg(self.conf_file)
        steps = cfg.get("move_step", "step").split(",")
        self.lcdNumber_move_step.display(int(steps[0]))

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Escape:
            self.close()

    def closeEvent(self, event):
        box = QMessageBox()
        box.setWindowTitle('提示')
        box.setIcon(QMessageBox.Question)
        box.setText('您确定要退出吗?')
        box.addButton('确定', QMessageBox.AcceptRole)
        no = box.addButton('取消', QMessageBox.RejectRole)
        box.setDefaultButton(no)
        reply = box.exec()
        if reply == QMessageBox.AcceptRole:
            event.accept()
            thread_move = thread_get_obj("move")
            if thread_move:
                thread_move.begin = False
            if self.parent():
                self.parent().pushButton_move.setDisabled(False)

        else:
            event.ignore()

    def eventFilter(self, obj, event):
        if not event.type() == QEvent.KeyRelease:
            return QDialog.eventFilter(self, obj, event)
        if event.modifiers() == Qt.ControlModifier and event.key() == Qt.Key_Up:  # ctrl ↑
            self.func_keyboard_event(who="up")
        elif event.modifiers() == Qt.ControlModifier and event.key() == Qt.Key_Down:  # ctrl ↓
            self.func_keyboard_event(who="down")
        elif event.modifiers() == Qt.ControlModifier and event.key() == Qt.Key_Left:  # ctrl ←
            self.func_keyboard_event(who="left")
        elif event.modifiers() == Qt.ControlModifier and event.key() == Qt.Key_Right:  # ctrl →
            self.func_keyboard_event(who="right")
        elif event.modifiers() == Qt.ControlModifier and event.key() == Qt.Key_Minus:  # ctrl -
            self.func_keyboard_step(who="-")
        elif event.modifiers() == Qt.ControlModifier and event.key() == Qt.Key_Equal:  # ctrl +
            self.func_keyboard_step(who="+")
        return QDialog.eventFilter(self, obj, event)

    def func_keyboard_btn(self, button):
        for btn in self.buttonGroup_keyboard_move.buttons():
            if btn == button:
                btn.setDisabled(True)
            else:
                btn.setDisabled(False)

    def get_key_btn_name(self):
        # 获取键盘控制切换的按钮
        for btn in self.buttonGroup_keyboard_move.buttons():
            if not btn.isEnabled():  # isEnable()提示button是否可以被用户点击
                return btn.objectName()

    def func_keyboard_event(self, who):
        btn_name = self.get_key_btn_name()
        if btn_name == "pushButton_free":
            return
        if btn_name == "pushButton_move_machine_sr":  # 移动原料盘
            self.func_keyboard_new_machine("sr", who)
            return
        if btn_name == "pushButton_move_machine_fg":  # 移动成品盘
            self.func_keyboard_new_machine("fg", who)
            return

    def func_keyboard_new_machine(self, sr_or_fg, who):
        cfg = file_get_cfg(self.conf_file)
        if sr_or_fg == "sr":
            x_label, y_label = "x", "y"
        else:
            x_label, y_label = "a", "b"
        x_min = cfg.getint("boundary", x_label + "_min")
        x_max = cfg.getint("boundary", x_label + "_max")
        y_min = cfg.getint("boundary", y_label + "_min")
        y_max = cfg.getint("boundary", y_label + "_max")
        thread_move = thread_get_obj("move")
        x, y = thread_move.points[x_label], thread_move.points[y_label]
        step = self.lcdNumber_move_step.intValue()
        if who == "up" and y_min <= y + step < y_max:
            val = y + step
            label = y_label
        elif who == "down" and y_min < y - step < y_max:
            val = y - step
            label = y_label
        elif who == "left" and x_min < x - step < x_max:
            val = x - step
            label = x_label
        elif who == "right" and x_min < x + step < x_max:
            val = x + step
            label = x_label
        else:
            return False
        udp_move_only(val, label)

    def func_keyboard_step(self, who):
        val = str(self.lcdNumber_move_step.intValue())
        cfg = file_get_cfg(self.conf_file)
        steps = cfg.get("move_step", "step").split(",")

        index = steps.index(val)
        if who == "-":
            index -= 1
        else:
            index += 1
        if 0 <= index < len(steps):
            self.lcdNumber_move_step.display(steps[index])

    def func_get_value_move(self, sr_or_fg):
        cfg = file_get_cfg(self.conf_file)
        if sr_or_fg == "sr":
            val = self.lineEdit_move_xy.text().strip()
            x_label, y_label = "x", "y"
            target = "xy"
        else:
            val = self.lineEdit_move_ab.text().strip()
            x_label, y_label = "a", "b"
            target = "ab"
        if not val:
            self.info("值不能为空")
            return
        pos = val.split(",")
        if len(pos) != 2:
            self.info("格式不正确")
            return
        x, y = pos
        try:
            x, y = int(x), int(y)
        except ValueError:
            self.info("请输入数字")
            return
        x_min = cfg.getint("boundary", x_label + "_min")
        x_max = cfg.getint("boundary", x_label + "_max")
        y_min = cfg.getint("boundary", y_label + "_min")
        y_max = cfg.getint("boundary", y_label + "_max")
        if x > x_max or x < x_min:
            self.info(x_label + "超出范围限制")
            return
        if y > y_max or y < y_min:
            self.info(y_label + "超出范围限制")
            return
        udp_move((x, y), target)

    def func_conf(self):
        conf = Setting(conf_file=self.conf_file)
        conf.display()

    def func_org(self, who):
        if who == "org_set":
            btn = self.pushButton_org_set
        elif who == "org_zero":
            btn = self.pushButton_org_zero
        else:
            return
        btn.setDisabled(True)
        ThreadOrg(thread_name=who, conf_file=self.conf_file, signal=self.signal.signal_org, btn=btn).start()


def run(alone=True, conf_file=None, parent=None):
    """
    启动
    :param alone: 独自启动不依赖其他QT
    :param conf_file: 配置文件路径
    :param parent: 依赖的父类
    :return:
    """
    app = None
    if alone:
        app = QApplication(sys.argv)
    if conf_file is None:
        conf_file = pathlib.Path(__file__).parent / "conf/move.ini"
    moves = Moves(conf_file=conf_file, parent=parent)
    cfg = file_get_cfg(conf_file)
    ip = cfg.get("receive_address", "ip")
    port = cfg.getint("receive_address", "port")
    thread_move = ThreadMove("move", address=(ip, port), signal=moves.signal.signal_move)
    thread_move.start()
    moves.show()
    if alone:
        sys.exit(app.exec_())


if __name__ == '__main__':
    run()
