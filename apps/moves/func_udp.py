# -*- coding: utf-8 -*-
__author__ = 'luointo'
__date__ = '2018/6/11 14:44'

import socket
import time
import datetime
import pathlib
from apps.common.func_func import file_get_cfg


# import sorter.core.varglobal as vg
# from sorter import XYAB_SEND_ADDRESS


# def udp_move_xyab(point, who, wait=True):
#     """
#     移动xyab
#     :param point: x, y坐标
#     :param who: xy或者ab
#     :param wait: 是否等待到位，True等
#     :return:
#     """
#     s_send = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
#     if who == "xy":
#         command = ".x" + str(point[0]) + ".y" + str(point[1]) + ".U87;"
#         x_s, y_s = "x", "y"
#         x_min = int(vg.local_conf["machine_x_min"]["value"])
#         x_max = int(vg.local_conf["machine_x_max"]["value"])
#         y_min = int(vg.local_conf["machine_y_min"]["value"])
#         y_max = int(vg.local_conf["machine_y_max"]["value"])
#     else:
#         command = ".a" + str(point[0]) + ".b" + str(point[1]) + ".U87;"
#         x_s, y_s = "a", "b"
#         x_min = int(vg.local_conf["machine_a_min"]["value"])
#         x_max = int(vg.local_conf["machine_a_max"]["value"])
#         y_min = int(vg.local_conf["machine_b_min"]["value"])
#         y_max = int(vg.local_conf["machine_b_max"]["value"])
#
#     px, py = point
#     if px < x_min or px > x_max or py < y_min or py > y_max:
#         # 如果超出边界值
#         return False
#     s_send.sendto(command.encode(), XYAB_SEND_ADDRESS)  # 发送数据
#     if not wait:
#         return
#     d1 = datetime.datetime.now()
#     d2 = d1 + datetime.timedelta(seconds=7)
#     while True:
#         x, y = vg.thread_four_road.points[x_s], vg.thread_four_road.points[y_s]
#         if x == px and y == py:
#             break
#         if not (datetime.datetime.now() - d2).seconds:
#             break
#         else:
#             time.sleep(0.0001)
#
#     # time.sleep(0.03)
#     return x, y


def get_send_address():
    filename = pathlib.Path(__file__).parent / "conf/move.ini"
    cfg = file_get_cfg(filename=filename)
    ip = cfg.get("send_address", "ip")
    port = cfg.getint("send_address", "port")
    return ip, port


def get_receive_address():
    filename = pathlib.Path(__file__).parent / "conf/move.ini"
    cfg = file_get_cfg(filename=filename)
    ip = cfg.get("receive_address", "ip")
    port = cfg.getint("receive_address", "port")
    return ip, port


def udp_move_only(val, who):
    # sr 或者 fg 只移动一个方向
    s_send = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    command = "." + str(who) + str(val) + ".U87;"
    s_send.sendto(command.encode(), get_send_address())  # 发送数据


def udp_move(point, who):
    s_send = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    if who == "xy":
        command = ".x" + str(point[0]) + ".y" + str(point[1]) + ".U87;"
    else:
        command = ".a" + str(point[0]) + ".b" + str(point[1]) + ".U87;"
    s_send.sendto(command.encode(), get_send_address())  # 发送数据


def udp_get_hostip(address):
    """
    获取ip地址
    :return:
    """
    # return "192.168.1.6", 552  # todo 正式上线修改

    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        s.settimeout(1)
        s.connect(address)
        s.settimeout(None)
        ip = s.getsockname()[0]
    except Exception as e:
        return False
        # logger = logging.getLogger('simple')
        # logger.error("4路控制器端口连接失败" + str(e))
    else:
        return ip, address[1]
    finally:
        s.close()


def udp_sned_data(data):
    # 向4路发送数据
    s_send = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s_send.sendto(data.encode(), get_send_address)  # 发送数据


def m2c(maxs, mins, lms):
    # vb6
    # m2c = m2c & Int((max - lms) * 1024 / 10 + 0.499) + 6500000 & "."
    # m2c = m2c & Int((min - lms) * 1024 / 10 + 0.499) + 6500000 & "."
    # m2c = m2c & Int((-lms) * 1024 / 10 + 0.499) + 6500000 & "."

    results = ""  # string
    results = results + str(int((maxs - lms) * 1024 / 10 + 0.499) + 6500000) + "."
    results = results + str(int((mins - lms) * 1024 / 10 + 0.499) + 6500000) + "."
    results = results + str(int((- lms) * 1024 / 10 + 0.499) + 6500000) + "."

    return results
