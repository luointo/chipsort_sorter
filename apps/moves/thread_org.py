# -*- coding: utf-8 -*-
__author__ = 'luointo'
__date__ = '2018/6/12 16:12'

"""
归零和原点设置
"""

import threading
import time
from apps.common.func_func import thread_get_obj, file_get_cfg, file_cfg_save
from apps.moves.func_udp import udp_sned_data


def m2c(max_val, min_val, lms):
    # vb6
    # m2c = m2c & Int((max - lms) * 1024 / 10 + 0.499) + 6500000 & "."
    # m2c = m2c & Int((min - lms) * 1024 / 10 + 0.499) + 6500000 & "."
    # m2c = m2c & Int((-lms) * 1024 / 10 + 0.499) + 6500000 & "."
    results = ""
    results = results + str(int((max_val - lms) * 1024 / 10 + 0.499) + 6500000) + "."
    results = results + str(int((min_val - lms) * 1024 / 10 + 0.499) + 6500000) + "."
    results = results + str(int((- lms) * 1024 / 10 + 0.499) + 6500000) + "."
    return results


class ThreadOrg(threading.Thread):
    def __init__(self, thread_name, conf_file, signal, btn):
        threading.Thread.__init__(self, name=thread_name)
        self.thread_name = thread_name
        self.conf_file = conf_file
        self.signal = signal
        self.btn = btn

    def run(self):
        cfg = file_get_cfg(filename=self.conf_file)
        x_org = cfg.getint("org", "x")
        y_org = cfg.getint("org", "y")
        a_org = cfg.getint("org", "a")
        b_org = cfg.getint("org", "b")

        if self.thread_name == "org_set":
            thread_move = thread_get_obj("move")
            x, y, a, b = thread_move.points.values()
            x_org += x
            y_org += y
            a_org += a
            b_org += b
            cfg.set("org", "x", str(x_org))
            cfg.set("org", "y", str(y_org))
            cfg.set("org", "a", str(a_org))
            cfg.set("org", "b", str(b_org))
            file_cfg_save(filename=self.conf_file, cfg=cfg)

        x_min = cfg.getint("boundary", "x_min")
        x_max = cfg.getint("boundary", "x_max")
        y_min = cfg.getint("boundary", "y_min")
        y_max = cfg.getint("boundary", "y_max")
        a_min = cfg.getint("boundary", "a_min")
        a_max = cfg.getint("boundary", "a_max")
        b_min = cfg.getint("boundary", "b_min")
        b_max = cfg.getint("boundary", "b_max")

        results = "m1." + m2c(x_min, x_max, x_org) + \
                  "m2." + m2c(-y_max, -y_min, y_org) + \
                  "m3." + m2c(a_min, a_max, a_org) + \
                  "m4." + m2c(-b_max, -b_min, b_org)

        udp_sned_data("A1,500,50000,400000,A2,500,50000,400000,A3,500,50000,400000,S1,5000000,160000000,")
        udp_sned_data(results)
        time.sleep(0.03)
        udp_sned_data("M")
        if self.thread_name == "zero":
            udp_sned_data("h1.1,h2.1,h3.1,h4.1.")
        self.signal.emit(self.btn)
