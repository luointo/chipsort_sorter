# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'boot.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow_Boot(object):
    def setupUi(self, MainWindow_Boot):
        MainWindow_Boot.setObjectName("MainWindow_Boot")
        MainWindow_Boot.resize(383, 411)
        self.centralwidget = QtWidgets.QWidget(MainWindow_Boot)
        self.centralwidget.setObjectName("centralwidget")
        self.pushButton_camera = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_camera.setGeometry(QtCore.QRect(110, 80, 151, 51))
        self.pushButton_camera.setObjectName("pushButton_camera")
        self.pushButton_move = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_move.setGeometry(QtCore.QRect(110, 150, 151, 51))
        self.pushButton_move.setObjectName("pushButton_move")
        self.pushButton_serial = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_serial.setGeometry(QtCore.QRect(110, 220, 151, 51))
        self.pushButton_serial.setObjectName("pushButton_serial")
        MainWindow_Boot.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow_Boot)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 383, 23))
        self.menubar.setObjectName("menubar")
        MainWindow_Boot.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow_Boot)
        self.statusbar.setObjectName("statusbar")
        MainWindow_Boot.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow_Boot)
        QtCore.QMetaObject.connectSlotsByName(MainWindow_Boot)

    def retranslateUi(self, MainWindow_Boot):
        _translate = QtCore.QCoreApplication.translate
        MainWindow_Boot.setWindowTitle(_translate("MainWindow_Boot", "启动界面"))
        self.pushButton_camera.setText(_translate("MainWindow_Boot", "摄像头"))
        self.pushButton_move.setText(_translate("MainWindow_Boot", "四路"))
        self.pushButton_serial.setText(_translate("MainWindow_Boot", "串口"))

