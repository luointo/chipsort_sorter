# -*- coding: utf-8 -*-
__author__ = 'luointo'
__date__ = '2018/6/8 17:09'

import sys
from PyQt5.QtWidgets import (QApplication, QMainWindow, QMessageBox)
from PyQt5.QtCore import Qt
from apps.boot.ui.boot import Ui_MainWindow_Boot
from apps import cameras
from apps import moves
from apps import serials


class Boot(QMainWindow, Ui_MainWindow_Boot):
    def __init__(self):
        super(Boot, self).__init__()
        self.setupUi(self)
        self.pushButton_camera.clicked.connect(self.func_cameras)
        self.pushButton_move.clicked.connect(self.func_moves)
        self.pushButton_serial.clicked.connect(self.func_serials)

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Escape:
            self.close()

    def closeEvent(self, event):
        box = QMessageBox()
        box.setWindowTitle('提示')
        box.setIcon(QMessageBox.Question)
        box.setText('您确定要退出吗?')
        box.addButton('确定', QMessageBox.AcceptRole)
        no = box.addButton('取消', QMessageBox.RejectRole)
        box.setDefaultButton(no)
        reply = box.exec()
        if reply == QMessageBox.AcceptRole:
            event.accept()
        else:
            event.ignore()

    def func_cameras(self):
        self.pushButton_camera.setDisabled(True)
        cameras.run(alone=False, parent=self)

    def func_moves(self):
        self.pushButton_move.setDisabled(True)
        moves.run(alone=False, parent=self)

    def func_serials(self):
        self.pushButton_serial.setDisabled(True)
        serials.run(alone=False, parent=self)


def run():
    app = QApplication(sys.argv)
    boot = Boot()
    boot.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    run()
