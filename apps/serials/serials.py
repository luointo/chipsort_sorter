# -*- coding: utf-8 -*-
__author__ = 'luointo'
__date__ = '2018/6/12 13:51'

import sys
import pathlib
import serial.tools.list_ports
from PyQt5.QtWidgets import (QApplication, QMessageBox)
from PyQt5.QtCore import (QObject, pyqtSignal, pyqtSlot, Qt, QRegExp)
from PyQt5.QtGui import (QTextCursor, QRegExpValidator)
from apps.serials.ui.serials import Ui_Dialog_Serial
from apps.serials.ui.electromagnet import Ui_Dialog_Electromagnet_Parameters
from apps.serials.ui.io_test import Ui_Dialog_Io_Test
from apps.serials.thread_serial import ThreadSerial
from apps.serials.thread_serial_load_data import ThreadSerialLoadData
from apps.serials.thread_serial_search_location import ThreadSerialSearchLocation
from apps.common.func_func import thread_get_obj, file_get_cfg, file_cfg_save
from apps.serials.func_serial import check_serial, close_serial
from apps.common.class_qt import BaseQDialog


class Signal(QObject):
    """
    信号
    """
    signal_serial = pyqtSignal(str)
    signal_search_needle_or_cap = pyqtSignal(object)

    def __init__(self, parent):
        super(Signal, self).__init__(parent=parent)
        self.signal_serial.connect(self.slot_serial)
        self.signal_search_needle_or_cap.connect(self.slot_search_needle_or_cap)

    @pyqtSlot(str)
    def slot_serial(self, data):
        self.parent().textEdit.append(data)
        self.parent().textEdit.moveCursor(QTextCursor.End)

    @pyqtSlot(object)
    def slot_search_needle_or_cap(self, btn):
        btn.setDisabled(False)
        if btn.objectName() == "pushButton_sr_needle":
            text = "探原料针位置完成"
        elif btn.objectName() == "pushButton_sr_cap":
            text = "探原料帽位置完成"
        else:
            text = "探成品帽位置完成"
        self.parent().info(text)


class ElectromagnetParameters(BaseQDialog, Ui_Dialog_Electromagnet_Parameters):
    # 电磁铁参数设置
    signal = pyqtSignal(str)

    def __init__(self, conf_file=None, parent=None):
        super(ElectromagnetParameters, self).__init__(parent=parent)
        self.setupUi(self)
        self.conf_file = conf_file
        self.signal.connect(self.slot_signal)
        self.pushButton_save.clicked.connect(self.save_conf)
        self.pushButton_cancel.clicked.connect(self.cancel_conf)
        self.line_edit_list = []  # 所有的文本框列表
        self.init()

    def init(self):
        if self.conf_file is None:
            self.conf_file = pathlib.Path(__file__).parent / "conf/electromagnet.ini"
        cfg = file_get_cfg(filename=self.conf_file)
        regex = QRegExp(r"^\d{10}$")
        for name, obj in vars(self).items():
            name_split = name.split("_", 1)  # 分割一次
            if name_split[0] != "lineEdit":
                continue
            obj.setValidator(QRegExpValidator(regex, self))
            option = name_split[1]
            section = option.rsplit("_", 1)[0]
            obj.setText(cfg.get(section=section, option=option))
            self.line_edit_list.append({"obj": obj, "section": section, "option": option})

    @pyqtSlot(str)
    def slot_signal(self, result):
        self.info(result)
        self.pushButton_save.setDisabled(False)

    def display(self):
        serial_load_data = thread_get_obj("serial_load_data")
        if serial_load_data:
            self.pushButton_save.setDisabled(True)
        self.exec_()

    def cancel_conf(self):
        cfg = file_get_cfg(filename=self.conf_file)
        for line_edit in self.line_edit_list:
            obj = line_edit["obj"]
            section = line_edit["section"]
            option = line_edit["option"]
            obj.setText(cfg.get(section=section, option=option))

    def save_conf(self):
        cfg = file_get_cfg(filename=self.conf_file)
        for line_edit in self.line_edit_list:
            obj = line_edit["obj"]
            section = line_edit["section"]
            option = line_edit["option"]
            val = obj.text().strip()
            if not val:
                self.info("不能有空值")
                return
            cfg.set(section, option, val)
        file_cfg_save(filename=self.conf_file, cfg=cfg)
        # 更新电磁铁参数
        serial_load_data = ThreadSerialLoadData("serial_load_data", self, "electromagnet")
        serial_load_data.start()
        self.pushButton_save.setDisabled(True)


class IoTest(BaseQDialog, Ui_Dialog_Io_Test):
    def __init__(self, io1_file, road1_file, road2_file, parent=None):
        super(IoTest, self).__init__(parent=parent)
        self.setupUi(self)
        self.io1_file = io1_file
        self.road1_file = road1_file
        self.road2_file = road2_file
        self.pushButton_s101.clicked.connect(lambda: self.parent().func_send_data_to_serial("s1 0 1,"))
        self.pushButton_s102.clicked.connect(lambda: self.parent().func_send_data_to_serial("s1 0 2,"))
        self.pushButton_s103.clicked.connect(lambda: self.parent().func_send_data_to_serial("s1 0 3,"))
        self.pushButton_s104.clicked.connect(lambda: self.parent().func_send_data_to_serial("s1 0 4,"))
        self.pushButton_s105.clicked.connect(lambda: self.parent().func_send_data_to_serial("s1 0 5,"))
        self.pushButton_s106.clicked.connect(lambda: self.parent().func_send_data_to_serial("s1 0 6,"))
        self.pushButton_s107.clicked.connect(lambda: self.parent().func_send_data_to_serial("s1 0 7,"))
        self.pushButton_s108.clicked.connect(lambda: self.parent().func_send_data_to_serial("s1 0 8,"))
        self.pushButton_s100.clicked.connect(lambda: self.parent().func_send_data_to_serial("s1 0 0,"))
        self.pushButton_s204.clicked.connect(lambda: self.parent().func_send_data_to_serial("s2 0 4,"))
        self.pushButton_s201.clicked.connect(lambda: self.parent().func_send_data_to_serial("s2 0 1,"))
        self.pushButton_s202.clicked.connect(lambda: self.parent().func_send_data_to_serial("s2 0 2,"))
        self.pushButton_s203.clicked.connect(lambda: self.parent().func_send_data_to_serial("s2 0 3,"))
        self.pushButton_h1000.clicked.connect(lambda: self.parent().func_send_data_to_serial("H 1,0,0,0,"))
        self.pushButton_h0100.clicked.connect(lambda: self.parent().func_send_data_to_serial("H 0,1,0,0,"))
        self.pushButton_r1.clicked.connect(lambda: self.func_send("r1"))
        self.pushButton_r2.clicked.connect(lambda: self.func_send("r2"))

    def display(self):

        serial_load_data = ThreadSerialLoadData("serial_load_data_io_open", self, "io_open")
        serial_load_data.start()
        self.exec_()

    def func_send(self, who):
        if who == "r1":
            line_edit = self.lineEdit_r1
            s = "R 1 "
        else:
            line_edit = self.lineEdit_r2
            s = "R 2 "
        text = line_edit.text().strip()
        if not text:
            return
        send_data = s + text + ","
        self.parent().func_send_data_to_serial(send_data)

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Escape:
            self.close()

    def closeEvent(self, event):
        # 关闭窗口后，载入road1和road2
        serial_load_data = ThreadSerialLoadData("serial_load_data_io_close", self, "io_close")
        serial_load_data.start()


class Serials(BaseQDialog, Ui_Dialog_Serial):
    """
    主界面
    """

    def __init__(self, electromagnet_file, road1_file, road2_file, io1_file, parent=None):
        super(Serials, self).__init__(parent=parent)
        self.setupUi(self)
        self.electromagnet_file = electromagnet_file
        self.road1_file = road1_file
        self.road2_file = road2_file
        self.io1_file = io1_file
        self.signal = Signal(parent=self)
        self.conf = ElectromagnetParameters(conf_file=self.electromagnet_file)
        self.pushButton_connect.clicked.connect(self.func_connect_serial)
        self.pushButton_disconnect.clicked.connect(self.func_disconnect_serial)
        self.pushButton_d9.clicked.connect(lambda: self.func_send_data_to_serial("d9."))
        self.pushButton_d8.clicked.connect(lambda: self.func_send_data_to_serial("d8."))
        self.pushButton_d0.clicked.connect(lambda: self.func_send_data_to_serial("d0."))
        self.pushButton_p1_1.clicked.connect(lambda: self.func_send_data_to_serial("P1,1,"))
        self.pushButton_p1_2.clicked.connect(lambda: self.func_send_data_to_serial("P1,2,"))
        self.pushButton_p1_3.clicked.connect(lambda: self.func_send_data_to_serial("P1,3,"))
        self.pushButton_p2_1.clicked.connect(lambda: self.func_send_data_to_serial("P2,1,"))
        self.pushButton_p2_2.clicked.connect(lambda: self.func_send_data_to_serial("P2,2,"))
        self.pushButton_p2_3.clicked.connect(lambda: self.func_send_data_to_serial("P2,3,"))
        self.pushButton_p3_1.clicked.connect(lambda: self.func_send_data_to_serial("P3,1,"))
        self.pushButton_p3_2.clicked.connect(lambda: self.func_send_data_to_serial("P3,2,"))
        self.pushButton_p3_3.clicked.connect(lambda: self.func_send_data_to_serial("P3,3,"))
        self.pushButton_p1_100.clicked.connect(lambda: self.func_send_data_to_serial("P1,100,"))
        self.pushButton_p2_100.clicked.connect(lambda: self.func_send_data_to_serial("P2,100,"))
        self.pushButton_p3_100.clicked.connect(lambda: self.func_send_data_to_serial("P3,100,"))
        self.pushButton_p1_0.clicked.connect(lambda: self.func_send_data_to_serial("P1,0,"))
        self.pushButton_p2_0.clicked.connect(lambda: self.func_send_data_to_serial("P2,0,"))
        self.pushButton_p3_0.clicked.connect(lambda: self.func_send_data_to_serial("P3,0,"))
        self.pushButton_c2.clicked.connect(lambda: self.func_send_data_to_serial("c2,"))
        self.pushButton_c3.clicked.connect(lambda: self.func_send_data_to_serial("c3,"))
        self.pushButton_electromagnet.clicked.connect(lambda: self.func_electromagnet_conf())
        self.pushButton_io_test.clicked.connect(lambda: self.func_io_test())
        self.pushButton_sr_needle.clicked.connect(lambda: self.func_search_needle_or_cap("sr_needle"))
        self.pushButton_sr_cap.clicked.connect(lambda: self.func_search_needle_or_cap("sr_cap"))
        self.pushButton_fg_cap.clicked.connect(lambda: self.func_search_needle_or_cap("fg_cap"))
        self.pushButton_text_clear.clicked.connect(lambda: self.func_text_clear())
        self.init()

    def init(self):
        self.pushButton_disconnect.setDisabled(True)
        plist = serial.tools.list_ports.comports()
        if len(plist) >= 1:
            for p in plist:
                self.comboBox.addItem(p.device)

    def func_connect_serial(self):
        serial_name = self.comboBox.currentText()
        if not serial_name:
            self.info("没有串口连接，请检查设备")
            return
        thread_serial = ThreadSerial("serial", self, serial_name)
        thread_serial.start()
        self.pushButton_connect.setDisabled(True)
        self.pushButton_disconnect.setDisabled(False)

    def func_disconnect_serial(self):
        close_serial()
        self.pushButton_connect.setDisabled(False)
        self.pushButton_disconnect.setDisabled(True)

    @check_serial
    def func_send_data_to_serial(self, data):
        thread_serial = thread_get_obj("serial")
        if not thread_serial:
            return
        thread_serial.send_data_to_serial(data)

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Escape:
            self.close()

    def closeEvent(self, event):
        box = QMessageBox()
        box.setWindowTitle('提示')
        box.setIcon(QMessageBox.Question)
        box.setText('您确定要退出吗?')
        box.addButton('确定', QMessageBox.AcceptRole)
        no = box.addButton('取消', QMessageBox.RejectRole)
        box.setDefaultButton(no)
        reply = box.exec()
        if reply == QMessageBox.AcceptRole:
            event.accept()
            close_serial()
            if self.parent():
                self.parent().pushButton_serial.setDisabled(False)
        else:
            event.ignore()

    @check_serial
    def func_electromagnet_conf(self):
        self.conf.display()

    @check_serial
    def func_io_test(self):
        conf = IoTest(io1_file=self.io1_file, road1_file=self.road1_file, road2_file=self.road2_file, parent=self)
        conf.display()

    @check_serial
    def func_search_needle_or_cap(self, who):
        if who == "sr_needle":
            btn = self.pushButton_sr_needle
        elif who == "sr_cap":
            btn = self.pushButton_sr_cap
        else:
            btn = self.pushButton_fg_cap
        btn.setDisabled(True)
        ThreadSerialSearchLocation("serial_search_location" + who,
                                   self.electromagnet_file,
                                   who,
                                   self.signal.signal_search_needle_or_cap,
                                   btn).start()

    def func_text_clear(self):
        self.textEdit.clear()


def run(alone=True, electromagnet_file=None, road1_file=None, road2_file=None, io1_file=None, parent=None):
    """
    启动
    :param alone: 独自启动不依赖其他QT
    :param electromagnet_file: 电磁铁配置文件
    :param road1_file: 小程序一路
    :param road2_file: 小程序二路
    :param io1_file: io1文件
    :param parent: 依赖的父类
    :return:
    """
    app = None
    if alone:
        app = QApplication(sys.argv)
    p = pathlib.Path(__file__).parent
    if electromagnet_file is None:
        electromagnet_file = p / "conf/electromagnet.ini"
    if road1_file is None:
        road1_file = p / "conf/road1.txt"
    if road2_file is None:
        road2_file = p / "conf/road2.txt"
    if io1_file is None:
        io1_file = p / "conf/io1.txt"
    ui = Serials(electromagnet_file, road1_file, road2_file, io1_file, parent)
    ui.show()
    if alone:
        sys.exit(app.exec_())


if __name__ == '__main__':
    run()

    # app = QApplication(sys.argv)
    # ex = ElectromagnetParameters()
    # ex.display()
    # sys.exit(app.exec_())

    # app = QApplication(sys.argv)
    # ex = IoTest()
    # ex.display()
    # sys.exit(app.exec_())
