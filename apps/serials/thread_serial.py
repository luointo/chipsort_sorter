# -*- coding: utf-8 -*-
__author__ = 'luointo'
__date__ = '2018/6/12 14:59'

"""
初始化电磁铁
"""

import threading
import serial
from apps.serials.func_serial import serial_load_electromagnet_data, serial_load_roads_data


class ThreadSerial(threading.Thread):
    def __init__(self, thread_name, ui, serial_name):
        threading.Thread.__init__(self, name=thread_name)
        self.ui = ui
        self.serial_name = serial_name
        self.begin = True
        self.ser = None
        self.current_row_data = None  # 串口返回的当前一行的数据

    def run(self):
        self.work()

    def work(self):
        self.ser = serial.Serial(self.serial_name, baudrate=115200, timeout=1)
        serial_load_electromagnet_data(electromagnet_file=self.ui.electromagnet_file, thread_serial=self)  # 载入电磁铁参数
        serial_load_roads_data(road1_file=self.ui.road1_file, road2_file=self.ui.road2_file, thread_serial=self)  # 载入road1和road2
        result = ""
        while self.begin:
            a = self.ser.read(1)
            a = bytes.decode(a)
            if a == "\r":
                self.current_row_data = result
                self.ui.signal.signal_serial.emit(result)
                result = ""
            else:
                result += a

    def send_data_to_serial(self, data):
        if not isinstance(data, bytes):
            data = data.encode()
        self.ser.write(data)


if __name__ == '__main__':
    ser = serial.Serial("com3", baudrate=115200, timeout=1)
    ser.write("aaa".encode())
