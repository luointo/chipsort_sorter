# -*- coding: utf-8 -*-
__author__ = 'luointo'
__date__ = '2018/6/14 9:27'

import threading
from apps.serials.func_serial import serial_load_roads_data, serial_load_io_data, serial_load_electromagnet_data


class ThreadSerialLoadData(threading.Thread):
    def __init__(self, thread_name, ui, who):
        threading.Thread.__init__(self, name=thread_name)
        self.ui = ui
        self.who = who

    def run(self):
        self.work()

    def work(self):

        if self.who == "io_open":
            serial_load_io_data(io_file=self.ui.io1_file)

        elif self.who == "io_close":
            serial_load_roads_data(road1_file=self.ui.road1_file, road2_file=self.ui.road2_file)

        elif self.who == "electromagnet":
            serial_load_electromagnet_data(electromagnet_file=self.ui.conf_file)
            signal = self.ui.signal
            signal.emit("保存成功")
        else:
            return
