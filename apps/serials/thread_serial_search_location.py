# -*- coding: utf-8 -*-
__author__ = 'luointo'
__date__ = '2018/6/15 9:05'

"""
探位置
"""

import threading
import time
import datetime
import numpy as np
from apps.common.func_func import thread_get_obj
from apps.common.func_func import file_get_cfg, file_cfg_save


class ThreadSerialSearchLocation(threading.Thread):
    def __init__(self, thread_name, conf_file, who, signal, btn):
        threading.Thread.__init__(self, name=thread_name)
        self.conf_file = conf_file
        self.who = who
        self.signal = signal
        self.btn = btn

    def run(self):
        self.work()

    def work(self):
        if self.who == "sr_needle":
            command = "P1,100,"
            row_index = 1
            cfg_section = "p1_2"
            cfg_option = "p1_2_1"
        elif self.who == "sr_cap":
            command = "P2,100,"
            row_index = 0
            cfg_section = "p2_2"
            cfg_option = "p2_2_1"
        else:
            command = "P3,100,"
            row_index = 2
            cfg_section = "p3_2"
            cfg_option = "p3_2_1"
        thread_serial = thread_get_obj("serial")
        if not thread_serial:
            return
        thread_serial.send_data_to_serial("d8.")
        time.sleep(1)
        thread_serial.send_data_to_serial(command)
        time.sleep(1)
        all_val = []
        d = datetime.datetime.now() + datetime.timedelta(seconds=5)
        while True:
            current_row_data = thread_serial.current_row_data
            val = int(current_row_data.split(";")[row_index].split(" ")[0])
            all_val.append(val)
            if not (datetime.datetime.now() - d).seconds:
                break
            else:
                time.sleep(0.1)
        all_val = np.array(all_val)
        mean_val = str(int(all_val.mean()))
        cfg = file_get_cfg(filename=self.conf_file)
        cfg.set(cfg_section, cfg_option, mean_val)
        file_cfg_save(filename=self.conf_file, cfg=cfg)
        self.signal.emit(self.btn)


if __name__ == '__main__':
    a = "2251 400,-1 0 0;2200 400,-3 0 0;1000 3000,15 0 0;"
