# -*- coding: utf-8 -*-
__author__ = 'luointo'
__date__ = '2018/6/14 17:14'

import time
import functools

from apps.common.func_func import thread_get_obj, file_get_cfg


def check_serial(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        obj = args[0]
        serial = thread_get_obj("serial")
        if not serial:
            obj.info("串口通信异常")
            return
        return func(*args, **kwargs)

    return wrapper


def close_serial():
    thread_serial = thread_get_obj("serial")
    if thread_serial:
        thread_serial.begin = False


def serial_load_electromagnet_data(electromagnet_file, thread_serial=None):
    if thread_serial is None:
        thread_serial = thread_get_obj("serial")
        if not thread_serial:
            return

    cfg = file_get_cfg(filename=electromagnet_file)
    # 更新电磁铁参数
    electromagnet_data = """
                P1,1,{p1_1_1},{p1_1_2},{p1_1_3},{p1_1_4},{p1_1_5},{p1_1_6},{p1_1_7},
                P1,2,{p1_2_1},{p1_2_2},{p1_2_3},{p1_2_4},{p1_2_5},{p1_2_6},{p1_2_7},
                P1,3,{p1_3_1},{p1_3_2},{p1_3_3},{p1_3_4},{p1_3_5},{p1_3_6},{p1_3_7},
                I1,{i1_1},{i1_2},{i1_3},{i1_4},
                P2,1,{p2_1_1},{p2_1_2},{p2_1_3},{p2_1_4},{p2_1_5},{p2_1_6},{p2_1_7},
                P2,2,{p2_2_1},{p2_2_2},{p2_2_3},{p2_2_4},{p2_2_5},{p2_2_6},{p2_2_7},
                P2,3,{p2_3_1},{p2_3_2},{p2_3_3},{p2_3_4},{p2_3_5},{p2_3_6},{p2_3_7},
                I2,{i2_1},{i2_2},{i2_3},{i2_4},
                P2,1,{p3_1_1},{p3_1_2},{p3_1_3},{p3_1_4},{p3_1_5},{p3_1_6},{p3_1_7},
                P2,2,{p3_2_1},{p3_2_2},{p3_2_3},{p3_2_4},{p3_2_5},{p3_2_6},{p3_2_7},
                P2,3,{p3_3_1},{p3_3_2},{p3_3_3},{p3_3_4},{p3_3_5},{p3_3_6},{p3_3_7},
                I3,{i3_1},{i3_2},{i3_3},{i3_4},
                v 1,{rotary_disk_one_1},{rotary_disk_one_2},{rotary_disk_one_3},
                v 2,{rotary_disk_two_1},{rotary_disk_two_2},{rotary_disk_two_3},
                """.format(
        p1_1_1=cfg.get("p1_1", "p1_1_1"),
        p1_1_2=cfg.get("p1_1", "p1_1_2"),
        p1_1_3=cfg.get("p1_1", "p1_1_3"),
        p1_1_4=cfg.get("p1_1", "p1_1_4"),
        p1_1_5=cfg.get("p1_1", "p1_1_5"),
        p1_1_6=cfg.get("p1_1", "p1_1_6"),
        p1_1_7=cfg.get("p1_1", "p1_1_7"),
        p1_2_1=cfg.get("p1_2", "p1_2_1"),
        p1_2_2=cfg.get("p1_2", "p1_2_2"),
        p1_2_3=cfg.get("p1_2", "p1_2_3"),
        p1_2_4=cfg.get("p1_2", "p1_2_4"),
        p1_2_5=cfg.get("p1_2", "p1_2_5"),
        p1_2_6=cfg.get("p1_2", "p1_2_6"),
        p1_2_7=cfg.get("p1_2", "p1_2_7"),
        p1_3_1=cfg.get("p1_3", "p1_3_1"),
        p1_3_2=cfg.get("p1_3", "p1_3_2"),
        p1_3_3=cfg.get("p1_3", "p1_3_3"),
        p1_3_4=cfg.get("p1_3", "p1_3_4"),
        p1_3_5=cfg.get("p1_3", "p1_3_5"),
        p1_3_6=cfg.get("p1_3", "p1_3_6"),
        p1_3_7=cfg.get("p1_3", "p1_3_7"),
        i1_1=cfg.get("i1", "i1_1"),
        i1_2=cfg.get("i1", "i1_2"),
        i1_3=cfg.get("i1", "i1_3"),
        i1_4=cfg.get("i1", "i1_4"),

        p2_1_1=cfg.get("p2_1", "p2_1_1"),
        p2_1_2=cfg.get("p2_1", "p2_1_2"),
        p2_1_3=cfg.get("p2_1", "p2_1_3"),
        p2_1_4=cfg.get("p2_1", "p2_1_4"),
        p2_1_5=cfg.get("p2_1", "p2_1_5"),
        p2_1_6=cfg.get("p2_1", "p2_1_6"),
        p2_1_7=cfg.get("p2_1", "p2_1_7"),
        p2_2_1=cfg.get("p2_2", "p2_2_1"),
        p2_2_2=cfg.get("p2_2", "p2_2_2"),
        p2_2_3=cfg.get("p2_2", "p2_2_3"),
        p2_2_4=cfg.get("p2_2", "p2_2_4"),
        p2_2_5=cfg.get("p2_2", "p2_2_5"),
        p2_2_6=cfg.get("p2_2", "p2_2_6"),
        p2_2_7=cfg.get("p2_2", "p2_2_7"),
        p2_3_1=cfg.get("p2_3", "p2_3_1"),
        p2_3_2=cfg.get("p2_3", "p2_3_2"),
        p2_3_3=cfg.get("p2_3", "p2_3_3"),
        p2_3_4=cfg.get("p2_3", "p2_3_4"),
        p2_3_5=cfg.get("p2_3", "p2_3_5"),
        p2_3_6=cfg.get("p2_3", "p2_3_6"),
        p2_3_7=cfg.get("p2_3", "p2_3_7"),
        i2_1=cfg.get("i2", "i2_1"),
        i2_2=cfg.get("i2", "i2_2"),
        i2_3=cfg.get("i2", "i2_3"),
        i2_4=cfg.get("i2", "i2_4"),

        p3_1_1=cfg.get("p3_1", "p3_1_1"),
        p3_1_2=cfg.get("p3_1", "p3_1_2"),
        p3_1_3=cfg.get("p3_1", "p3_1_3"),
        p3_1_4=cfg.get("p3_1", "p3_1_4"),
        p3_1_5=cfg.get("p3_1", "p3_1_5"),
        p3_1_6=cfg.get("p3_1", "p3_1_6"),
        p3_1_7=cfg.get("p3_1", "p3_1_7"),
        p3_2_1=cfg.get("p3_2", "p3_2_1"),
        p3_2_2=cfg.get("p3_2", "p3_2_2"),
        p3_2_3=cfg.get("p3_2", "p3_2_3"),
        p3_2_4=cfg.get("p3_2", "p3_2_4"),
        p3_2_5=cfg.get("p3_2", "p3_2_5"),
        p3_2_6=cfg.get("p3_2", "p3_2_6"),
        p3_2_7=cfg.get("p3_2", "p3_2_7"),
        p3_3_1=cfg.get("p3_3", "p3_3_1"),
        p3_3_2=cfg.get("p3_3", "p3_3_2"),
        p3_3_3=cfg.get("p3_3", "p3_3_3"),
        p3_3_4=cfg.get("p3_3", "p3_3_4"),
        p3_3_5=cfg.get("p3_3", "p3_3_5"),
        p3_3_6=cfg.get("p3_3", "p3_3_6"),
        p3_3_7=cfg.get("p3_3", "p3_3_7"),
        i3_1=cfg.get("i3", "i3_1"),
        i3_2=cfg.get("i3", "i3_2"),
        i3_3=cfg.get("i3", "i3_3"),
        i3_4=cfg.get("i3", "i3_4"),

        rotary_disk_one_1=cfg.get("rotary_disk_one", "rotary_disk_one_1"),
        rotary_disk_one_2=cfg.get("rotary_disk_one", "rotary_disk_one_2"),
        rotary_disk_one_3=cfg.get("rotary_disk_one", "rotary_disk_one_3"),
        rotary_disk_two_1=cfg.get("rotary_disk_two", "rotary_disk_two_1"),
        rotary_disk_two_2=cfg.get("rotary_disk_two", "rotary_disk_two_2"),
        rotary_disk_two_3=cfg.get("rotary_disk_two", "rotary_disk_two_3"),
    )

    for line in electromagnet_data.splitlines():
        line = line.strip()
        if not line:
            continue
        line = " " + line
        thread_serial.send_data_to_serial(line)
        time.sleep(0.01)


def serial_load_roads_data(road1_file, road2_file, thread_serial=None):
    if thread_serial is None:
        thread_serial = thread_get_obj("serial")
        if not thread_serial:
            return
    for filename in [road1_file, road2_file]:
        with open(file=filename, mode="r", encoding="utf-8") as f:
            for line in f:
                line = line.split("'/")[0]  # 去掉注释
                line = line.strip()
                if not line:
                    continue
                line = " " + line
                thread_serial.send_data_to_serial(line)
                time.sleep(0.01)


def serial_load_io_data(io_file, thread_serial=None):
    if thread_serial is None:
        thread_serial = thread_get_obj("serial")
        if not thread_serial:
            return
    with open(file=io_file, mode="r", encoding="utf-8") as f:
        for line in f:
            line = line.strip()
            if not line:
                continue
            line = " " + line
            thread_serial.send_data_to_serial(line)
            time.sleep(0.01)
